install node js and mongodb .
npm install
run node app.js

<!-- Register the user : -->

method: post
api: /register
body :
{ userName,
middleName,
lastName,
gender,
userEmail,
password}

<!-- login -->

method:post
api:/login

body:
{
userEmail,
password
}

it will give you jwt token

<!-- socket io : -->

Event: EMIT
NAME: storeUserData
params as json: {userEmail:"rukkumani@gmail.com"},

<!-- for group chat u have to join the group -->

Event :EMIT
NAME:join
params as json:{room:"room1"}

<!-- to massage in a group : -->

Event :EMIT
NAME:message
params as json:{room:"room1",userEmail:"rukkumani@gmail.com",message:"HI all welcome "}

<!-- to send the private message -->

Event :EMIT
NAME:privateSMS
params as json:{userEmail:"rukkumani@gmail.com",receiverEmail:"hello.rukkumani@gmail.com",message:"HI this private smmssss "}

to get all the group and indivitual sms:

method:get
params:userEmail
api:/userAllConversation/:userEmail
