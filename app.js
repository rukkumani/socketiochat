const express = require("express"),
  config = require("./config/config"),
  glob = require("glob"),
  mongoose = require("mongoose"),
  app = express();

mongoose.connect(config.db, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
});
var db = mongoose.connection;
db.on("error", function() {
  throw new Error("unable to connect to database at " + config.db);
});

const models = glob.sync(config.root + "/app/models/*.js");
models.forEach(function(model) {
  require(model);
});
const chatRooms = mongoose.model("chatRooms"),
  privateChat = mongoose.model("privateChat");

const server = app.listen(config.port, function() {
  console.log("Express server listening on port " + config.port);
});

var usernames = [];
var io = require("socket.io")(server);

io.on("connection", socket => {
  socket.on("storeUserData", function(data) {
    if (data.userEmail) {
      var userInfo = {};
      userInfo.userEmail = data.userEmail;
      userInfo.SocketId = socket.id;
      usernames.push(userInfo);
    } else {
      socket.emit("newmessage", "Please provide mail id");
    }
  });

  // join the room for group sms

  socket.on("join", data => {
    if (data && data.room) {
      socket.join(data.room);
      chatRooms.findOne({ name: data.room }, (err, rooms) => {
        if (err) {
          return false;
        }
        if (!rooms) {
          const newChatRoom = new chatRooms({ name: data.room, messages: [] });
          newChatRoom.save();
        }
      });
    } else {
      socket.emit("newmessage", "please specify the room");
    }
  });

  function userExist(data) {
    return usernames.filter(
      user => user.SocketId == socket.id && user.userEmail == data.userEmail
    );
  }

  function receiverExist(data) {
    return usernames.filter(user => user.userEmail == data.receiverEmail);
  }

  //send group sms
  socket.on("message", data => {
    const checkUser = userExist(data);
    if (checkUser && checkUser.length) {
      io.sockets.in(data.room).emit("newmessage", {
        user: data.userEmail,
        message: data.message
      });
      chatRooms.updateOne(
        { name: data.room },
        {
          $push: { messages: { sender: data.userEmail, message: data.message } }
        },
        (err, res) => {
          if (err) {
            console.log(err);
            return false;
          }
        }
      );
    } else {
      socket.emit("newmessage", "Please Register yourself");
    }
  });

  socket.on("privateSMS", data => {
    if (
      data &&
      Object.keys(data) &&
      data.userEmail &&
      data.receiverEmail &&
      data.message
    ) {
      const receiver = receiverExist(data);
      const sender = userExist(data);
      if (receiver && receiver.length && sender && sender.length) {
        const newPrivateChate = new privateChat(data);
        newPrivateChate
          .save()
          .then(value => {
            socket.to(receiver[0].SocketId).emit("newmessage", data);
          })
          .catch(err => {
            socket.emit("newmessage", "something went wrong");
          });
      } else {
        socket.emit("newmessage", "Please correct sender or receiver");
      }
    } else {
      socket.emit("newmessage", "Please provide all the data");
    }
  });

  socket.on("disconnect", function(sockets) {
    var len = usernames.length;

    for (var i = 0; i < len; i++) {
      var user = usernames[i];

      if (user.socketId == socket.id) {
        usernames.splice(i, 1);
        break;
      }
    }
  });
});

require("./config/express")(app, config, io);
