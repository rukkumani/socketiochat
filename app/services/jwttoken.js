let mongoose = require("mongoose"),
  userModel = mongoose.model("user"),
  jwt = require("jsonwebtoken");

module.exports = function login(req, res) {
  let { userEmail, password } = req.body;
  let secret = "doodleblue";

  if (userEmail && password) {
    userModel
      .findOne({ userEmail: userEmail, password: password })
      .then(value => {
        if (value) {
          let token = jwt.sign({ userEmail: userEmail }, secret, {
            expiresIn: "24h" // expires in 24 hours
          });
          // return the JWT token for the future API calls
          res.json({
            success: true,
            message: "Authentication successful!",
            token: token
          });
        } else {
          res.send(403).json({
            success: false,
            message: "Incorrect username or password"
          });
        }
      })
      .catch(err => {
        console.log(err);
        res.send({ data: "something went wrong" });
      });
  } else {
    res.send(400).json({
      success: false,
      message: "Authentication failed! Please check the request"
    });
  }
};
