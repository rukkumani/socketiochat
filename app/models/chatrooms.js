let mongoose = require("mongoose"),
  chatRoomsSchema = new mongoose.Schema({
    name: { type: String, required: true },
    messages: [
      {
        sender: { type: String, required: true },
        message: { type: String, required: true },
        timestamps: { type: Date, default: Date.now() }
      }
    ]
  });
mongoose.model("chatRooms", chatRoomsSchema);
