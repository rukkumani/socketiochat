let mongoose = require("mongoose"),
  registerUserSchema = new mongoose.Schema({
    userName: { type: String, required: true },
    middleName: { type: String, required: true },
    lastName: { type: String, required: true },
    gender: { type: String, required: true },
    userEmail: { type: String, required: true, unique: true },
    password: String
  });
mongoose.model("user", registerUserSchema);
