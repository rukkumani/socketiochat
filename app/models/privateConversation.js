let mongoose = require("mongoose"),
  privateChatSchema = new mongoose.Schema(
    {
      userEmail: { type: String, required: true },
      receiverEmail: { type: String, required: true },
      message: { type: String, required: true }
    },
    { timestamps: true }
  );
mongoose.model("privateChat", privateChatSchema);
