const express = require("express"),
  router = express.Router(),
  mongoose = require("mongoose"),
  userModel = mongoose.model("user"),
  generateToken = require("../services/jwttoken.js");
verifyToken = require("../services/jwtverify");

// app/routes.js

module.exports = function(app, io, liveConnection) {
  app.use("/", router);
  socket = io;
  connected_user = liveConnection;
};

router.post("/login", [generateToken]);

router.post("/register", (req, res) => {
  if (
    req.body &&
    Object.keys(req.body).length &&
    req.body.userEmail &&
    req.body.userName &&
    req.body.middleName &&
    req.body.gender &&
    req.body.password
  ) {
    userModel
      .findOne({ userEmail: req.body.userEmail })
      .then(async userData => {
        if (userData) {
          res.send({ data: "user already exists" });
        } else {
          let newUser = new userModel(req.body);
          newUser
            .save()
            .then(
              value => {
                res.send({ data: "success" });
              },
              reject => {
                res.send({ data: "error" });
              }
            )
            .catch(err => {
              res.send({ data: "error" });
            });
        }
      })
      .catch(err => {
        res.send({ data: "error" });
      });
  } else {
    res.send({ data: "please provide all the details" });
  }
});

router.get("/", verifyToken.checkToken, (req, res) => {
  res.send({ data: "HI welcome" });
});

