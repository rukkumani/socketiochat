const express = require("express"),
  router = express.Router(),
  mongoose = require("mongoose"),
  chatRooms = mongoose.model("chatRooms"),
  privateChat = mongoose.model("privateChat"),
  // userModel = mongoose.model("user"),
  generateToken = require("../services/jwttoken.js"),
verifyToken = require("../services/jwtverify");

module.exports = function(app, io, liveConnection) {
  app.use("/", router);
  socket = io;
  connected_user = liveConnection;
};

router.get("/userAllConversation/:userEmail", (req, res) => {
  chatRooms.aggregate(
    [
      {
        $match: {
          messages: {
            $elemMatch: {
              sender: req.params.userEmail
            }
          }
        }
      },
      {
        $lookup: {
          from: "privatechats",
          localField: "messages.sender",
          foreignField: "receiverEmail",
          as: "conversation"
        }
      }
    ],
    (err, result) => {
      // console.log(err);
      // console.log(result[0].conversation);
      if(err)
      res.send({data:"error"});
      res.send(result[0]);
    }
  );
});
