const glob = require("glob"),
  cors = require("cors"),
  logger = require("morgan"),
  cookieParser = require("cookie-parser"),
  bodyParser = require("body-parser"),
  compress = require("compression"),
  methodOverride = require("method-override");

module.exports = function(app, config, io) {
  const env = process.env.NODE_ENV || "production";
  app.locals.ENV = env;
  app.use(logger("dev"));
  app.set("views", config.root + "/app/views");
  app.set("view engine", "jade");
  app.use(bodyParser.json({ limit: "500MB" }));
  app.use(
    bodyParser.urlencoded({
      limit: "500MB",
      extended: true
    })
  );
  app.use(cookieParser());
  app.use(compress());
  app.use(methodOverride());
  app.use(cors());

  var controllers = glob.sync(config.root + "/app/controllers/*.js");
  controllers.forEach(function(controller) {
    require(controller)(app,io);
  });

  app.get("/", (req, res) => {
    res.send("hiiii");
  });

  app.use(function(req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
  });

  if (app.get("env") === "development") {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render("error", {
        message: err.message,
        error: err,
        title: "error"
      });
    });
  }

  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: {},
      title: "error"
    });
  });
};
