const path = require("path"),
  rootPath = path.normalize(__dirname + "/.."),
  chalk = require("chalk"),
  env = process.env.NODE_ENV || "production",
  dbHost = process.env.DB_HOST_NAME,
  dbPort = process.env.DB_PORT;

var config = {
  production: {
    root: rootPath,
    port: 4001 || dbPort,
    db: "mongodb://localhost:27017/doodle"
  }
};
module.exports = config[env];
logConfiguration();
function logConfiguration() {
  console.log(chalk.styles.green.open + chalk.styles.green.close);
  console.log(
    "\n\n ---------------------Configuration in Use --------------------------"
  );
  console.log(config[env]);
}
